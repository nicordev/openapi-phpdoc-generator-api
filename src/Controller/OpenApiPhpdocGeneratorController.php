<?php

declare(strict_types=1);

namespace App\Controller;

use Nicordev\OpenApiPhpdocGenerator\OpenApiPhpdocGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class OpenApiPhpdocGeneratorController extends AbstractController
{
    #[Route('/generate', name: 'openapi-phpdoc-generator-generate', methods: ['POST'])]
    public function generate(Request $request, OpenApiPhpdocGenerator $openApiPhpdocGenerator)
    {
        $contentParts = json_decode($request->getContent(false), true);

        if (null === $contentParts) {
            return new JsonResponse([
                'errors' => [
                    [
                        'message' => 'Missing json content to handle.'
                    ]
                ],
            ], Response::HTTP_BAD_REQUEST);
        }

        return new Response($openApiPhpdocGenerator->generate($contentParts), Response::HTTP_OK, [
            'Content-Type' => 'text/plain'
        ]);
    }
}
