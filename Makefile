ifdef filter
	FILTER = --filter=${filter}
endif

test:
	php bin/phpunit tests ${FILTER}

.PHONY: test