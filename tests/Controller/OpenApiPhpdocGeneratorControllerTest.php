<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OpenApiPhpdocGeneratorControllerTest extends WebTestCase
{
    public function testGenerate_wrongMethod(): void
    {
        $client = static::createClient();
        $client->request('GET', '/generate');
        self::assertResponseStatusCodeSame(405);
    }

    public function testGenerate_withoutData(): void
    {
        $client = static::createClient();
        $client->request('POST', '/generate');
        self::assertResponseStatusCodeSame(400);
    }

    public function testGenerate_withData(): void
    {
        $requestContent = file_get_contents(__DIR__.'/../fixtures/dummy_root_object.json');
        $expected = file_get_contents(__DIR__ . '/../expected/testGenerate_withData.txt');
        $client = static::createClient();
        $client->request(
            'POST',
            '/generate',
            [],
            [],
            [],
            $requestContent,
        );
        $content = $client->getResponse()
            ->getContent();
        self::assertResponseIsSuccessful();
        self::assertEquals($expected, $content);
    }
}
